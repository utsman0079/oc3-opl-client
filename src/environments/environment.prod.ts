export const environment = {
  production: true,
  baseUrl: '',
  otsukaApi: 'https://myapps.aio.co.id/otsuka-api/api/',
  defaultauth: 'fackbackend',
  firebaseConfig: {
    apiKey: '',
    authDomain: '',
    databaseURL: '',
    projectId: '',
    storageBucket: '',
    messagingSenderId: '',
    appId: '',
    measurementId: ''
  }
};

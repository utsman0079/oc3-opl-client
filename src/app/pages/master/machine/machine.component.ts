import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject, debounceTime } from 'rxjs';
import { CommonService } from 'src/app/core/services/common.service';
import { restApiService } from 'src/app/core/services/rest-api.service';
import { Const } from 'src/app/core/static/const';
import { SortDirection } from 'src/app/global-component';

@Component({
  selector: 'app-machine',
  templateUrl: './machine.component.html',
  styleUrls: ['./machine.component.scss']
})
export class MachineComponent {
  machineData: any[] = []
  machineId!: number | null

  areaData: any[] = []

  isLoading = false;
  breadCrumbItems!: Array<{}>

  searchTerm = ''
  searchSubject = new Subject<string>()

  totalItems = 0
  currentPage = 1
  pageSize = 10
  totalPages!: number
  sortColumn = 'id'
  sortDirection: SortDirection = 'asc'

  form = {
    name: '',
    detail: '',
    area_id: ''
  }
  isFormInvalid = false

  constructor(private apiService: restApiService, private modalService: NgbModal, public common: CommonService) {
    this.breadCrumbItems = [
      { label: 'Master', active: false },
      { label: 'Machine Area', active: true }
    ];
    this.searchSubject.pipe(debounceTime(350)).subscribe((term) => {
      this.searchMachineByPagination(term, this.currentPage, this.pageSize)
    })
  }

  ngOnInit() {
    this.getArea()
    this.searchMachineByPagination(this.searchTerm, this.currentPage, this.pageSize)
  }
  searchMachineByPagination(term: string, currentPage: number, pageSize: number) {
    this.isLoading = true;
    this.apiService.searchMachineByPagination(term, currentPage, pageSize, this.sortColumn, this.sortDirection).subscribe({
      next: (res: any) => {
        this.isLoading = false;
        this.machineData = res.data
        this.totalItems = res.total
      },
      error: (err: HttpErrorResponse) => {
        this.isLoading = false;
        this.common.showServerErrorAlert(Const.ERR_GET_MSG("Machine"), err.statusText)
      }
    })
  }

  getArea() {
    this.isLoading = true;
    this.apiService.getArea().subscribe({
      next: (res: any) => {
        this.isLoading = false;
        const data: any[] = res.data
        const lines = this.common.getUniqueData(data, "line_id").map(line => ({ line_id: line.line_id, line: line.line } as any))
        lines.forEach(line => {
          let area: any[] = []
          data.forEach(item => {
            if (item.line_id === line.line_id) {
              area.push(item)
            }
          })
          line.area = area
        })
        this.areaData = lines
      },
      error: (err: HttpErrorResponse) => {
        this.isLoading = false;
        this.common.showServerErrorAlert(Const.ERR_GET_MSG("Area"), err.statusText)
      }
    })
  }
  calculateStartingIndex(index: number): number {
    return (this.currentPage - 1) * this.pageSize + index + 1;
  }
  
  openModal(template: any, data?: any) {
    if (data) {
      this.machineId = data.id
      this.form.name = data.name
      this.form.detail = data.detail
      this.form.area_id = data.area_id
    }
    this.modalService.open(template, { centered: true }).result.then(
      (result) => this.resetModalValue(),
      (reason) => this.resetModalValue()
    )
  }

  resetModalValue() {
    this.form.name = ''
    this.form.detail = ''
    this.form.area_id = ''
    this.machineId = null
    this.isFormInvalid = false
  }

  onDeleteMachine(id: number, machine: string) {
    this.common.showDeleteWarningAlert(Const.ALERT_DEL_MSG(machine)).then((result) => {
      if (result.isConfirmed) {
        this.apiService.updateMachine(id, { is_removed: 1 }).subscribe({
          next: (res: any) => {
            this.searchSubject.next(this.searchTerm)
            this.modalService.dismissAll()
          },
          error: (err: HttpErrorResponse) => {
            this.common.showErrorAlert(Const.ERR_DELETE_MSG("Machine"), err.statusText)
          }
        })
      }
    })
  }

  onSaveChanges() {
    if (this.form.name && this.form.area_id) {
      this.isFormInvalid = false;
      if (!this.machineId) {
        this.apiService.insertMachine(this.form).subscribe({
          next: (res: any) => {
            this.searchSubject.next(this.searchTerm)
            this.modalService.dismissAll()
          },
          error: (err: HttpErrorResponse) => this.common.showErrorAlert(Const.ERR_INSERT_MSG("Machine"), err.statusText)
        })
      } else {
        this.apiService.updateMachine(this.machineId, this.form).subscribe({
          next: (res: any) => {
            this.searchSubject.next(this.searchTerm)
            this.modalService.dismissAll()
          },
          error: (err: HttpErrorResponse) => this.common.showErrorAlert(Const.ERR_UPDATE_MSG("Machine"), err.statusText)
        })
      }
    } else {
      this.isFormInvalid = true;
    }
  }

  onSort(column: string) {
    if (this.sortColumn === column) {
      this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortColumn = column;
      this.sortDirection = 'asc';
    }
    this.searchSubject.next(this.searchTerm)
  }
}

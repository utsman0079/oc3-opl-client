import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject, debounceTime } from 'rxjs';
import { CommonService } from 'src/app/core/services/common.service';
import { restApiService } from 'src/app/core/services/rest-api.service';
import { Const } from 'src/app/core/static/const';
import { SortDirection } from 'src/app/global-component';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss']
})
export class AreaComponent {
  areaData: any[] = []
  areaId!: number | null

  lineData: any[] = []

  isLoading = false;
  breadCrumbItems!: Array<{}>

  searchTerm = ''
  searchSubject = new Subject<string>()

  totalItems = 0
  currentPage = 1
  pageSize = 10
  totalPages!: number
  sortColumn = 'id'
  sortDirection: SortDirection = 'asc'

  form = {
    name: '',
    detail: '',
    line_id: ''
  }
  isFormInvalid = false

  constructor(private apiService: restApiService, private modalService: NgbModal, public common: CommonService) {
    this.breadCrumbItems = [
      { label: 'Master', active: false },
      { label: 'Area', active: true }
    ];
    this.searchSubject.pipe(debounceTime(350)).subscribe((term) => {
      this.searchAreaByPagination(term, this.currentPage, this.pageSize)
    })
  }

  ngOnInit() {
    this.getFactoryLine()
    this.searchAreaByPagination(this.searchTerm, this.currentPage, this.pageSize)
  }
  searchAreaByPagination(term: string, currentPage: number, pageSize: number) {
    this.isLoading = true;
    this.apiService.searchAreaByPagination(term, currentPage, pageSize, this.sortColumn, this.sortDirection).subscribe({
      next: (res: any) => {
        this.isLoading = false;
        this.areaData = res.data
        this.totalItems = res.total
      },
      error: (err: HttpErrorResponse) => {
        this.isLoading = false;
        this.common.showServerErrorAlert(Const.ERR_GET_MSG("Area"), err.statusText)
      }
    })
  }

  getFactoryLine() {
    this.isLoading = true;
    this.apiService.getFactoryLine().subscribe({
      next: (res: any) => {
        this.isLoading = false;
        this.lineData = res.data
      },
      error: (err: HttpErrorResponse) => {
        this.isLoading = false;
        this.common.showServerErrorAlert(Const.ERR_GET_MSG("Factory Line"), err.statusText)
      }
    })
  }
  calculateStartingIndex(index: number): number {
    return (this.currentPage - 1) * this.pageSize + index + 1;
  }
  
  openModal(template: any, data?: any) {
    if (data) {
      this.areaId = data.id
      this.form.name = data.name
      this.form.detail = data.detail
      this.form.line_id = data.line_id
    }
    this.modalService.open(template, { centered: true }).result.then(
      (result) => this.resetModalValue(),
      (reason) => this.resetModalValue()
    )
  }

  resetModalValue() {
    this.form.name = ''
    this.form.detail = ''
    // this.form.line_id = ''
    this.areaId = null
    this.isFormInvalid = false
  }

  onDeleteArea(id: number, area: string) {
    this.common.showDeleteWarningAlert(Const.ALERT_DEL_MSG(area)).then((result) => {
      if (result.isConfirmed) {
        this.apiService.updateArea(id, { is_removed: 1 }).subscribe({
          next: (res: any) => {
            this.searchSubject.next(this.searchTerm)
            this.modalService.dismissAll()
          },
          error: (err: HttpErrorResponse) => {
            this.common.showErrorAlert(Const.ERR_DELETE_MSG("Area"), err.statusText)
          }
        })
      }
    })
  }

  onSaveChanges() {
    if (this.form.name && this.form.line_id) {
      this.isFormInvalid = false;
      if (!this.areaId) {
        this.apiService.insertArea(this.form).subscribe({
          next: (res: any) => {
            this.searchSubject.next(this.searchTerm)
            this.modalService.dismissAll()
          },
          error: (err: HttpErrorResponse) => this.common.showErrorAlert(Const.ERR_INSERT_MSG("Area"), err.statusText)
        })
      } else {
        this.apiService.updateArea(this.areaId, this.form).subscribe({
          next: (res: any) => {
            this.searchSubject.next(this.searchTerm)
            this.modalService.dismissAll()
          },
          error: (err: HttpErrorResponse) => this.common.showErrorAlert(Const.ERR_UPDATE_MSG("Area"), err.statusText)
        })
      }
    } else {
      this.isFormInvalid = true;
    }
  }

  onSort(column: string) {
    if (this.sortColumn === column) {
      this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortColumn = column;
      this.sortDirection = 'asc';
    }
    this.searchSubject.next(this.searchTerm)
  }
}

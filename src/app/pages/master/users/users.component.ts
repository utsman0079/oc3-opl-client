import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, OperatorFunction, Subject, debounceTime, distinctUntilChanged, of, switchMap, tap } from 'rxjs';
import { CommonService } from 'src/app/core/services/common.service';
import { restApiService } from 'src/app/core/services/rest-api.service';
import { Const } from 'src/app/core/static/const';
import { SortDirection } from 'src/app/global-component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {
  roleData: any[] = []
  usersData: any[] = []
  userId!: number | null
  lineData: any[] = [{ id: -1, name: 'All Access Granted' }]

  isLoading = false;
  breadCrumbItems!: Array<{}>;

  searchTerm = '';
  searchSubject = new Subject<string>();

  totalItems = 0;
  currentPage = 1;
  pageSize = 10;
  sortColumn = 'user_id';
  sortDirection: SortDirection = 'asc'

  form = { nik: '', name: '', email: '', role_id: '', area_access: '' }
  isFormInvalid = false;

  employee: any
  employeeFormatter = (employee: any) => employee.nik ? `${employee.nik} - ${employee.employee_name}` : ""
  employeeSearching = false
  employeeSearchLength: number | null = null
  employeeSearch: OperatorFunction<string, any[]> = (text$: Observable<any>) => {
    const employees = text$.pipe(
      debounceTime(300), distinctUntilChanged(), tap(() => (this.employeeSearching = true)),
      switchMap((query: string) => {
        if (query.length >= 3) {
          this.employeeSearchLength = null
          return this.apiService.getEmployeeData(query)
        } else {
          this.employeeSearchLength = query.length
          return of([] as any[])
        }
      }), tap(() => this.employeeSearching = false)
    )

    return employees
  }
  
  constructor (private apiService: restApiService, private modalService: NgbModal, public common: CommonService) {
    this.breadCrumbItems = [
      { label: 'Master', active: false },
      { label: 'Users', active: true }
    ];
    this.searchSubject.pipe(debounceTime(350)).subscribe((term) => {
      this.searchUsersByPagination(term, this.currentPage, this.pageSize)
    })
  }

  ngOnInit() {
    // this.getUserRoles()
    this.getFactoryLine()
    this.searchUsersByPagination(this.searchTerm, this.currentPage, this.pageSize)
    
    this.notMultiLineReset()

  }
  // getUserRoles() {
  //   this.isLoading = true
  //   this.apiService.getUserRole().subscribe({
  //     next: (res) => {
  //       this.isLoading = false
  //       this.roleData = res.data
  //       this.form.role_id = this.roleData[0].role_id
  //     },
  //     error: (err) => {
  //       this.isLoading = false
  //       this.common.showServerErrorAlert(Const.ERR_GET_MSG("Role"), err)
  //     }
  //   })
  // }

  searchUsersByPagination(term: string, currentPage: number, pageSize: number) {
    this.isLoading = true;
    this.apiService.searchUsersByPagination(term, currentPage, pageSize, this.sortColumn, this.sortDirection).subscribe({
      next: (res: any) => {
        this.isLoading = false;
        this.usersData = res.data
        this.totalItems = res.total
      },
      error: (err: HttpErrorResponse) => {
        this.isLoading = false;
        this.common.showServerErrorAlert(Const.ERR_GET_MSG("User"), err.statusText)
      }
    })
  }

  getFactoryLine() {
    this.isLoading = true
    this.apiService.getFactoryLine().subscribe({
      next: (res) => {
        this.isLoading = false;
        res.data.forEach((data: any) => this.lineData.push(data))
        this.lineData.forEach(item => {
          delete item['detail']
          delete item['is_removed']
          item.checked = false
        })
      },
      error: (err: HttpErrorResponse) => {
        this.isLoading = false
        this.common.showServerErrorAlert(Const.ERR_GET_MSG("Factory Line"), err.statusText)
      }
    })
  }

  isLineAccessDisabled = false
  selectedLineAccess = ''
  onLineAccessSelected(event$: any, data: any) {
    data.checked = event$.target.checked
    if (data.name === "All Access Granted") {
      this.isLineAccessDisabled = event$.target.checked
      this.lineData.forEach(item => {
        if (item.name !== "All Access Granted") {
          item.checked = false
        }
      })
    } else {
      this.isLineAccessDisabled = false
    }
    
    const line: string[] = [...this.lineData].filter((item) => item.checked).map(item => item.name)
    this.selectedLineAccess = line.join(", ")

    if (this.lineData.every(item => !item.checked)) {
      this.form.area_access = ''
    } else {
      this.form.area_access = JSON.stringify([...this.lineData].filter((item) => item.checked).map(data => ({id: data.id, name: data.name})))
    }
    
  }

  setLineAccess(lineAccess: string): string {
    const lineData: string[] = JSON.parse(lineAccess).map((obj: any) => obj.name)
    return lineData.join(", ")
  }

  onEmployeeFormSearch(event: any) {
    setTimeout(() => {
      if (this.employee) {
        this.form.nik = this.employee.nik
        this.form.email = this.employee.email
        this.form.name = this.employee.employee_name
      }
    }, 50)
  }

  calculateStartingIndex(index: number): number {
    return (this.currentPage - 1) * this.pageSize + index + 1;
  }

  openModal(template: any, data?: any) {
    if (data) {
      Object.keys(this.form).forEach(key => {
        (this.form as any)[key] = data[key]
      })
      const lineAccess: any[] = JSON.parse(data.area_access)
      this.lineData.forEach(item => {
        lineAccess.forEach(obj => {
          if (item.name === obj.name) {
            item.checked = true
          }
        })
      })
      if (lineAccess.length === 1 && lineAccess[0].name == 'All Access Granted') {
        this.isLineAccessDisabled = true
      }

      const line: string[] = [...this.lineData].filter((item) => item.checked).map(item => item.name)
      this.selectedLineAccess = line.join(", ")

      this.userId = data.user_id
    }
    
    this.modalService.open(template, { centered: true, size: 'lg' }).result.then(
      (result) => this.resetModalValue(),
      (reason) => this.resetModalValue()
    )
  }

  resetModalValue() {
    Object.keys(this.form).forEach(key => {
      (this.form as any)[key] = '' 
    })
    this.lineData.forEach(item => item.checked = false)
    this.userId = null
    this.employee = null
    // this.isLineAccessDisabled = false
    // this.selectedLineAccess = ''
    this.notMultiLineReset()
  }

  onDeleteUser(userId: number, user: string) {
    this.common.showDeleteWarningAlert(Const.ALERT_DEL_MSG(user)).then((result) => {
      if (result.isConfirmed) {
        this.isLoading = true
        this.apiService.deleteUser(userId).subscribe({
          next: (res: any) => {
            this.isLoading = false
            this.searchSubject.next(this.searchTerm)
            this.modalService.dismissAll()
          },
          error: (err: HttpErrorResponse) => {
            this.isLoading = false
            this.common.showErrorAlert(Const.ERR_DELETE_MSG("User"), err.statusText)
          }
        })
      }
    })
  }

  onSaveChanges() {
    const isFormFilled = Object.keys(this.form).every(key => (this.form as any)[key])
    if (isFormFilled) {
      this.isFormInvalid = false
      this.isLoading = true
      if (this.userId) {
        this.apiService.updateUser(this.userId, this.form).subscribe({
          next: (res) => {
            this.isLoading = false
            this.searchSubject.next(this.searchTerm)
            this.modalService.dismissAll()
          },
          error: (err: HttpErrorResponse) => {
            this.isLoading = false
            this.common.showErrorAlert(Const.ERR_UPDATE_MSG("User"), err.status === 400 ? err.error.data.message : err.statusText)
          }
        })
      } else {
        this.apiService.insertUser(this.form).subscribe({
          next: (res: any) => {
            this.isLoading = false
            this.searchSubject.next(this.searchTerm)
            this.modalService.dismissAll()
          },
          error: (err: HttpErrorResponse) => {
            this.isLoading = false
            this.common.showErrorAlert(Const.ERR_INSERT_MSG("User"), err.status === 400 ? err.error.data.message : err.statusText)
          }
        })
      }

    } else {
      this.isFormInvalid = true
    }
  }

  onSort(column: string) {
    if (this.sortColumn === column) {
      this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortColumn = column;
      this.sortDirection = 'asc';
    }
    this.searchSubject.next(this.searchTerm)
  }

  notMultiLineReset() {
    this.isLineAccessDisabled = true
    this.selectedLineAccess = 'All Access Granted'
    this.form.area_access = JSON.stringify([{"id":-1,"name":"All Access Granted"}])
  }

}

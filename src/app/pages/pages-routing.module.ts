import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StarterComponent } from './extrapages/starter/starter.component';
import { UsersComponent } from './master/users/users.component';
import { LineComponent } from './master/line/line.component';
import { AreaComponent } from './master/area/area.component';
import { MachineComponent } from './master/machine/machine.component';

// Component pages

const routes: Routes = [
    {
      path: "",
      component: StarterComponent
    },
    {
      path: "master/line",
      component: LineComponent
    },
    {
      path: "master/area",
      component: AreaComponent
    },
    {
      path: "master/machine",
      component: MachineComponent
    },
    {
      path: "master/users",
      component: UsersComponent
    },
    {
      path: '', loadChildren: () => import('./extrapages/extraspages.module').then(m => m.ExtraspagesModule)
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }

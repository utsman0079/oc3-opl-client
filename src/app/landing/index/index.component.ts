import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { CommonService } from 'src/app/core/services/common.service';
import { restApiService } from 'src/app/core/services/rest-api.service';
import { TokenStorageService } from 'src/app/core/services/token-storage.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})

/**
 * Index Component
 */
export class IndexComponent implements OnInit {

  currentSection = 'home';
  showNavigationArrows: any;
  showNavigationIndicators: any;

  areaData: any[] = []
  lineData: any[] = []
  isLoading = false;
  selectedLine = {
    id: 0,
    slug: '',
    name: ''
  }

  userData: any

  constructor(
    private apiService: restApiService, 
    public common: CommonService, 
    private tokenService: TokenStorageService,
    public router: Router,
    private route: ActivatedRoute,
    private authService: AuthenticationService
  ) {
      this.userData = tokenService.getUser()
  }

  async ngOnInit() {

    this.route.paramMap.subscribe(async params => {
      const line = params.get('line')
      const request = this.apiService.requestHttpGet("public", `line/slug/${line}`)
      this.isLoading = true;
      const lineData: any[] = await this.common.handleHTTPResponse(request, "Line", "GET").then(res => res.data).finally(() => this.isLoading = false)

      if (lineData.length > 0) {
        this.selectedLine = {
          id: lineData[0].id,
          slug: lineData[0].slug,
          name: lineData[0].name
        }

        await this.getAreaByLineId(this.selectedLine.id)
        
      } else {
        this.router.navigate(['./pages/not-found'])
      } 
    })
  }

  async getAreaByLineId(lineId: number) {
    const request = this.apiService.requestHttpGet("public", `area/line/${lineId}`)
    this.isLoading = true;
    this.areaData = await this.common.handleHTTPResponse(request, "Area", "GET")
      .then((res) => res.data).finally(() => this.isLoading = false)
  }

  /**
   * Window scroll method
   */
  // tslint:disable-next-line: typedef
  windowScroll() {
    const navbar = document.getElementById('navbar');
    if (document.body.scrollTop > 40 || document.documentElement.scrollTop > 40) {
      navbar?.classList.add('is-sticky');
    }
    else {
      navbar?.classList.remove('is-sticky');
    }

    // Top Btn Set
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
      (document.getElementById("back-to-top") as HTMLElement).style.display = "block"
    } else {
      (document.getElementById("back-to-top") as HTMLElement).style.display = "none"
    }
  }

   /**
   * Section changed method
   * @param sectionId specify the current sectionID
   */
    onSectionChange(sectionId: string) {
      this.currentSection = sectionId;
    }

     /**
   * Toggle navbar
   */
  toggleMenu() {
    document.getElementById('navbarSupportedContent')?.classList.toggle('show');
  }

  // When the user clicks on the button, scroll to the top of the document
  topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  onLogout() {
    this.authService.logout()
    location.reload()
  }

}

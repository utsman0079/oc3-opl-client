import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Observable, OperatorFunction, Subject, debounceTime, distinctUntilChanged, filter, map, merge, of, switchMap, tap } from 'rxjs';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { CommonService } from 'src/app/core/services/common.service';
import { restApiService } from 'src/app/core/services/rest-api.service';
import { TokenStorageService } from 'src/app/core/services/token-storage.service';
import { Const } from 'src/app/core/static/const';
import { GlobalComponent, SortDirection } from 'src/app/global-component';

@Component({
  selector: 'app-opl',
  templateUrl: './opl.component.html',
  styleUrls: ['./opl.component.scss']
})
export class OplComponent {

  currentSection = 'home'

  selectedAreaData: any
  selectedAreaName: string = ''

  oplData: any[] = []
  oplId: number | null = null

  defaultMachineData: any[] = []
  selectedMachineFilter: any
  selectedMachineForm: any
  isOnMachineFilter = false

  searchTerm = ''
  searchSubject = new Subject<string>()

  totalItems = 0
  currentPage = 1
  pageSize = 10
  totalPages!: number
  sortColumn = 'created_at'
  sortDirection: SortDirection = 'desc'

  form = {
    file_name: '',
    file_size: '',
    machine_id: '',
    detail: '',
    uploaded_by: '',
    created_at: null
  }

  isFormInvalid = false;

  isLoading = false;
  userData: any

  currentUser: string = ''

  slug = ''
  machineSlug = ''

  dateRange = {
    from: '',
    to: ''
  }

  selectedOplName: string | null = null
  selectedOplFile: File | null = null;

  @ViewChild('instance', { static: true }) instance!: NgbTypeahead;

  focus$ = new Subject<string>();
	click$ = new Subject<string>();

  oplRawLink = (fileName: string, newFileName = '') => `${GlobalComponent.API_URL}file/opl/${fileName}?name=${newFileName}`
  oplDownloadLink = (fileName: string, newFileName = '') => {
    window.open(`${GlobalComponent.API_URL}file/opl/${fileName}?name=${newFileName}`)
  }

  employee: any
  employeeFormatter = (employee: any) => employee.nik ? `${employee.nik} - ${employee.employee_name}` : ""
  employeeSearching = false
  employeeSearchLength: number | null = null
  employeeSearch: OperatorFunction<string, any[]> = (text$: Observable<any>) => {
    const employees = text$.pipe(
      debounceTime(300), distinctUntilChanged(), tap(() => (this.employeeSearching = true)),
      switchMap((query: string) => {
        if (query.length >= 3) {
          this.employeeSearchLength = null
          return this.apiService.getEmployeeData(query)
        } else {
          this.employeeSearchLength = query.length
          return of([] as any[])
        }
      }), tap(() => this.employeeSearching = false)
    )

    return employees
  }

  machineAreaFormatter = (machine: any) => machine.name
  machineAreaSearch: OperatorFunction<string, any[]> = (text$: Observable<any>) => {
    const debouncedText$ = text$.pipe(debounceTime(300), distinctUntilChanged())
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      switchMap((query: string) => 
        query === '' ? of(this.modalService.hasOpenModals() ? this.defaultMachineData.filter(obj => obj.id !== -1) : this.defaultMachineData) 
          : this.searchMachine(this.selectedAreaData.id, query)), 
    )
  }

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    public common: CommonService,
    private apiService: restApiService,
    private tokenService: TokenStorageService,
    private modalService: NgbModal,
    private authService: AuthenticationService,
    private datePipe: DatePipe
  ) {
    this.userData = tokenService.getUser()
    if (this.userData) {
      this.currentUser = `${this.userData.nik} - ${this.userData.name}`
    }
    this.searchSubject.pipe(debounceTime(350)).subscribe(term => {
      if (this.isOnMachineFilter) {
        this.searchOplPerMachineByPagination(term, this.machineSlug)
      } else {
        this.searchOplByPagination(term)
      }
    })
  }

  async ngOnInit() {
    this.route.params.subscribe(async params => {
      if (params) {
        this.slug = params['area']
        this.isLoading = true;
        const request = this.apiService.requestHttpGet(`public`, `area/slug/${this.slug}`)
        this.selectedAreaData = await this.common.handleHTTPResponse(request, "Area", "GET")
          .then((res) => (res.data as any[]).length > 0 ? res.data[0] : null)
          .finally(() => this.isLoading = false)
        if (this.selectedAreaData) {
          this.selectedAreaName = `${this.selectedAreaData.name} - ${this.selectedAreaData.line}` || ''
          await this.getMachineByAreaId(this.selectedAreaData.id)
          await this.searchOplByPagination(this.searchTerm)
        } else {
          this.router.navigate(['../pages/not-found'])
        }
        
      }
    })
  }

  async getMachineByAreaId(areaId: number) {
    const request = this.apiService.requestHttpGet("public", `machine/area/${areaId}`)
    this.isLoading = true;
    this.defaultMachineData = await this.common.handleHTTPResponse(request, "Machine", "GET")
      .then((res) => res.data)
      .finally(() => this.isLoading = false)
  }

  searchMachine(areaId: number, term: string) {
    return this.apiService.requestHttpGet("public", `machine/search-area/${areaId}?search=${term}`).pipe(
      map((res) => {
        const machines: any[] = []
        if (!this.modalService.hasOpenModals()) {
          machines.push({id: -1, name: '--All Machine Area--', slug: null});
        }
        (res.data as any[]).forEach((machine) => machines.push(machine));
        return machines.filter((v) => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1)
          .filter((data) => new RegExp(term, 'mi').test(`${data.name}`))
          .slice(0, 10)
      })
    )
  }

  async searchOplByPagination(term: string) {
    const request = this.apiService.requestHttpGet(
      "public", 
      `opl/pagination/area/${this.slug}?from=${this.dateRange.from}&to=${this.dateRange.to}&page=${this.currentPage}&pageSize=${this.pageSize}&sortColumn=${this.sortColumn}&sortDirection=${this.sortDirection}&search=${term}`
    )
    this.isLoading = true;
    this.oplData = await this.common.handleHTTPResponse(request, "OPL", "GET")
      .then(res => {
        this.totalItems = res.total
        return res.data
      }).finally(() => this.isLoading = false)
  }

  async searchOplPerMachineByPagination(term: string, machineSlug: string) {
    const request = this.apiService.requestHttpGet(
      "public", 
      `opl/pagination/machine/${machineSlug}?from=${this.dateRange.from}&to=${this.dateRange.to}&page=${this.currentPage}&pageSize=${this.pageSize}&sortColumn=${this.sortColumn}&sortDirection=${this.sortDirection}&search=${term}`
    )
    this.isLoading = true;
    this.oplData = await this.common.handleHTTPResponse(request, "OPL Per Machine", "GET")
     .then(res => {
        this.totalItems = res.total
        return res.data
      }).finally(() => this.isLoading = false)
  }

  calculateStartingIndex(index: number): number {
    return (this.currentPage - 1) * this.pageSize + index + 1;
  }

  onSelectedFile(event: any) {
    this.selectedOplFile = event.target.files[0]
  }

  openModal(template: any, data?: any) {
    if (data) {
      this.oplId = data.opl_id
      this.selectedMachineForm = { id: data.machine_id, name: data.machine, slug: data.machine_slug }
      if (data.uploaded_by) {
        this.employee = { 
          nik: `${data.uploaded_by}`.split(' - ')[0], 
          employee_name: `${data.uploaded_by}`.split(' - ')[1] 
        }
      }
      Object.keys(this.form).forEach(key => {
        if (key !== 'created_at') {
          (this.form as any)[key] = data[key]
        } else {
          this.form[key] = this.datePipe.transform(data.created_at, 'yyyy-MM-dd HH:mm:ss') as any
        }
      })
    } else {
      this.form.uploaded_by = this.currentUser
      this.employee = {
        nik: this.userData.nik,
        employee_name: this.userData.name
      }
    }

    this.modalService.open(template, { centered: true, size: 'lg' }).result.then(
      (result) => this.resetModalValue(),
      (reason) => this.resetModalValue()
    )
  }

  resetModalValue() {
    Object.keys(this.form).forEach(key => {
      if (key !== 'created_at') {
        (this.form as any)[key] = ''
      } else if (key === 'created_at') {
        this.form[key] = null
      }
    })
    this.employee = null
    this.selectedMachineForm = null
    this.oplId = null
    this.isFormInvalid = false
    this.selectedOplFile = null
    this.selectedOplName = null
  }

  // Admin only
  async onDeleteOpl(id: number, opl: string) {
    this.common.showDeleteWarningAlert(Const.ALERT_DEL_MSG(opl)).then(async result => {
      if (result.isConfirmed) {
        const request = this.apiService.requestHttpPut(`master`, `opl`, id, { is_removed: 1 })
        this.isLoading = true;
        await this.common.handleHTTPResponse(request, "OPL", "DELETE").then((res) => {
          this.searchSubject.next(this.searchTerm)
          this.modalService.dismissAll()
        }).finally(() => this.isLoading = false)
      }
    })
  }

  onSaveChanges() {
    const isAllFromFilled = (this.form.machine_id !== '' && this.form.detail !== '' && this.form.uploaded_by !== '' && this.employee) 

    if (isAllFromFilled) {
      this.isFormInvalid = false

      if (!this.oplId) {

        if (!this.selectedOplFile) {
          this.isFormInvalid = true
        } else {
          const formData = new FormData()
          formData.append("file", this.selectedOplFile)
  
          const uploadRequest = this.apiService.requestUploadFile(`master`, `opl/upload`, formData)
          this.isLoading = true
  
          this.common.handleHTTPResponse(uploadRequest, "Uploaded OPL", "INSERT").then((res) => {
            this.form.file_name = `${res.data.file_name}`
            this.form.file_size = `${parseInt(res.data.file_size) || 0}`
            this.isLoading = true
    
            const request = this.apiService.requestHttpPost('master', `opl`, this.form)
            this.common.handleHTTPResponse(request, "OPL", "INSERT")
              .then((res) => {
                this.searchSubject.next(this.searchTerm)
                this.modalService.dismissAll()
              }).finally(() => this.isLoading = false)
    
          }).finally(() => this.isLoading = false)
        }

      } else {
        const data: any = { ...this.form }
        delete data['file_name']
        delete data['file_size']
        const request = this.apiService.requestHttpPut('master', `opl`, this.oplId, data)
        this.isLoading = true;
        this.common.handleHTTPResponse(request, "OPL", "EDIT")
          .then((res) => {
            this.searchSubject.next(this.searchTerm)
            this.modalService.dismissAll()
          })
          .finally(() => this.isLoading = false)
      }

      

    } else {
      this.isFormInvalid = true
    }
  }

  onChangeDateRange(event: any) {
    const value = event.target.value as string
    const datesArray = value.split(' to ');
    if (datesArray.length === 2) {
      const [fromDate, toDate] = datesArray
      this.dateRange = { from: fromDate, to: toDate }
      this.searchSubject.next(this.searchTerm)
    }
  }

  onSort(column: string) {
    if (this.sortColumn === column) {
      this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortColumn = column;
      this.sortDirection = 'asc';
    }
    this.searchSubject.next(this.searchTerm)
  }

  onBackToArea() {
    const lineSlug = this.selectedAreaData.line_slug || ''
    this.router.navigate([`./${lineSlug}`])
  }

  onEmployeeFormSearch(event: any) {
    setTimeout(() => {
      if (this.employee) {
        this.form.uploaded_by = `${this.employee.nik} - ${this.employee.employee_name}`
      }
    }, 50)
  }

  onMachineAreaFilter(event: any) {
    if (event.target.value) {
      const machineData = JSON.parse(event.target.value)
      if (machineData.id !== -1) {
        this.isOnMachineFilter = true;
        this.machineSlug = machineData.slug
      } else {
        this.isOnMachineFilter = false;
      }
      this.searchSubject.next(this.searchTerm)
    }
    
  }

  onMachineFilterSearch(event: any) {
    setTimeout(() => {
      if (this.selectedMachineFilter) {
        this.isOnMachineFilter = this.selectedMachineFilter.id !== -1
      }
      this.searchSubject.next(this.searchTerm)
    }, 50)
  }

  onMachineFormSearch(event: any) {
    setTimeout(() => {
      if (this.selectedMachineForm) {
        this.form.machine_id = `${this.selectedMachineForm.id}`
      }
    }, 50)
  }

  onDateAddedChange(event: any) {
    const date = event.target.value
    if (date) {
      this.form.created_at = date
    } else {
      this.form.created_at = null
    }
  }

  onSectionChange(sectionId: string) {
    this.currentSection = sectionId;
  }

  windowScroll() {
    const navbar = document.getElementById('navbar');
    if (document.body.scrollTop > 40 || document.documentElement.scrollTop > 40) {
      navbar?.classList.add('is-sticky');
    }
    else {
      navbar?.classList.remove('is-sticky');
    }

    // Top Btn Set
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
      (document.getElementById("back-to-top") as HTMLElement).style.display = "block"
    } else {
      (document.getElementById("back-to-top") as HTMLElement).style.display = "none"
    }
  }

  topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  toggleMenu() {
    document.getElementById('navbarSupportedContent')?.classList.toggle('show');
  }

  onLogout() {
    this.authService.logout()
    location.reload()
  }

}

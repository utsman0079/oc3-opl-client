import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { IndexComponent } from './index/index.component';
import { RouterModule } from '@angular/router';

import {
  NgbCarouselModule, NgbTooltipModule, NgbCollapseModule,
  NgbPaginationModule,
  NgbModalModule,
  NgbDropdownModule,
  NgbTypeahead
} from '@ng-bootstrap/ng-bootstrap';

import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

import { LandingRoutingModule } from "./landing-routing.module";
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import { SharedModule } from '../shared/shared.module';
import { OplComponent } from './index/opl/opl.component';
import { FormsModule } from '@angular/forms';
import { FlatpickrModule } from 'angularx-flatpickr';
import { SelectLineComponent } from './select-line/select-line.component';

@NgModule({
  declarations: [
    IndexComponent,
    OplComponent,
    SelectLineComponent
  ],
  imports: [
    CommonModule,
    NgbCarouselModule,
    LandingRoutingModule,
    SharedModule,
    RouterModule,
    NgbTooltipModule,
    NgbCollapseModule,
    ScrollToModule.forRoot(),
    NgxLoadingModule,
    FormsModule,
    NgbPaginationModule,
    NgbModalModule,
    FlatpickrModule,
    NgbDropdownModule,
    NgbTypeahead
  ],
  providers: [DatePipe]
})
export class LandingModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component Pages
import { IndexComponent } from "./index/index.component";
import { OplComponent } from './index/opl/opl.component';
import { SelectLineComponent } from './select-line/select-line.component';

const routes: Routes = [
  {
    path: "",
    component: SelectLineComponent
  },
  {
    path: ":line",
    component: IndexComponent
  },
  {
    path: "opl/:area",
    component: OplComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class LandingRoutingModule { }

import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { CommonService } from 'src/app/core/services/common.service';
import { restApiService } from 'src/app/core/services/rest-api.service';
import { TokenStorageService } from 'src/app/core/services/token-storage.service';

@Component({
  selector: 'app-select-line',
  templateUrl: './select-line.component.html',
  styleUrls: ['./select-line.component.scss']
})
export class SelectLineComponent {
  currentSection = 'home';

  lineData: any[] = []
  isLoading = false;

  selectedArea = {
    id: 0,
    slug: '',
    name: '',
  }

  userData: any

  constructor(
    private apiService: restApiService, 
    public common: CommonService, 
    private tokenService: TokenStorageService,
    public router: Router,
    private route: ActivatedRoute,
    private authService: AuthenticationService
  ) {
      this.userData = tokenService.getUser()
  }

  async ngOnInit() {
    await this.getFactoryLine()
  }

  async getFactoryLine() {
    const request = this.apiService.requestHttpGet("public", `line`)
    this.isLoading = true;
    this.lineData = await this.common.handleHTTPResponse(request, "Line", "GET")
      .then((res) => res.data).finally(() => this.isLoading = false)
  }

  /**
   * Window scroll method
   */
  // tslint:disable-next-line: typedef
  windowScroll() {
    const navbar = document.getElementById('navbar');
    if (document.body.scrollTop > 40 || document.documentElement.scrollTop > 40) {
      navbar?.classList.add('is-sticky');
    }
    else {
      navbar?.classList.remove('is-sticky');
    }

    // Top Btn Set
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
      (document.getElementById("back-to-top") as HTMLElement).style.display = "block"
    } else {
      (document.getElementById("back-to-top") as HTMLElement).style.display = "none"
    }
  }

   /**
   * Section changed method
   * @param sectionId specify the current sectionID
   */
    onSectionChange(sectionId: string) {
      this.currentSection = sectionId;
    }

     /**
   * Toggle navbar
   */
  toggleMenu() {
    document.getElementById('navbarSupportedContent')?.classList.toggle('show');
  }

  // When the user clicks on the button, scroll to the top of the document
  topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  onLogout() {
    this.authService.logout()
    location.reload()
  }
}

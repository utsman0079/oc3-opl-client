import { MenuItem } from './menu.model';

let menuId = 0;
export function createMenuId() {
  return menuId++;
}

export const MENU: MenuItem[] = [
  {
    id: createMenuId(),
    label: 'MAIN',
    isTitle: true
  },
  {
    id: createMenuId(),
    label: 'Home',
    icon: 'ri-dashboard-fill',
    link: '/admin',
  },
  {
    id: createMenuId(),
    label: 'MASTER DATA',
    isTitle: true
  },
  {
    id: createMenuId(),
    label: 'Manage Line',
    icon: 'ri-building-2-fill',
    link: 'master/line',
  },
  {
    id: createMenuId(),
    label: 'Manage Area',
    icon: 'ri-building-3-fill',
    link: 'master/area',
  },
  {
    id: createMenuId(),
    label: 'Manage Machine Area',
    icon: 'ri-scan-2-fill',
    link: 'master/machine',
  },
  {
    id: createMenuId(),
    label: 'Manage Users',
    icon: 'ri-user-3-fill',
    link: 'master/users',
  },

];

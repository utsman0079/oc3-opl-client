import { environment } from "src/environments/environment";

export const GlobalComponent = {

    // General URL
    API_URL: `${environment.baseUrl}api/`,
    
    // Auth Api
    AUTH_API:`${environment.baseUrl}api/auth/`,

    // Master URL
    MASTER_API_URL: `${environment.baseUrl}api/master/`,

    // Public URL
    PUBLIC_API_URL: `${environment.baseUrl}api/public/`,

    // AIO Api
    AIO_API: `${environment.otsukaApi}`,

    // Local Storage
    REFRESH_TOKEN_KEY: 'OPL_refreshToken',
    TOKEN_KEY: 'OPL_token',
    USER_KEY: 'OPL_currentUser',

    // Crypto Token
    USER_ENCRYPTION_KEY: 'intern-prodOC3-1',
    ACCESS_TOKEN_ENCRYPTION_KEY: 'intern-prodOC3-2',
    REFRESH_TOKEN_ENCRYPTION_KEY: 'intern-prodOC3-3',

    headerToken : {'Authorization': `Bearer ${localStorage.getItem('token')}`},

    // Products Api
    product:'apps/product',
    productDelete:'apps/product/',

    // Orders Api
    order:'apps/order',
    orderId:'apps/order/',

    // Customers Api
    customer:'apps/customer',
   
}

export type ApiType = "master" | "auth" | "public" | "file"
export type SortDirection = "asc" | "desc"
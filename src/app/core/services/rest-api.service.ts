import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, map, of, tap, throwError } from 'rxjs';
import { GlobalComponent, ApiType, SortDirection } from "../../global-component";


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class restApiService {
  constructor(private http: HttpClient) { }

  cache: any[] = []
  
  resetCachedData(cachedData?:string) {
    if (cachedData) {
      const index = this.cache.findIndex((item) => item[cachedData])
      if (index >= 0) {
        this.cache.splice(index, 1)
      }
    } else {
      this.cache.splice(0)
    }
  }

  isCachedDataExists(cachedData:string): boolean {
    const data = this.cache.find((item) => item[cachedData])
    return data ? true : false
  }

  getCachedData(cachedData: string): any {
    const data = this.cache.find((item) => item[cachedData])
    if (data) {
      return data[cachedData]
    } else throwError(`${cachedData} not found`)
  }

  setCachedData(cacheKey: string, data: any) {
    this.cache.push({[cacheKey]: data})
  }

  requestCachedHttpGet(type: ApiType, urlParams: string, cacheKey: string): Observable<any> {
    if (this.isCachedDataExists(cacheKey)) {
      return of(this.getCachedData(cacheKey))
    } else {
      return this.http.get(GlobalComponent.API_URL +`${type}/` + urlParams, httpOptions).pipe(
        tap((data) => this.setCachedData(cacheKey, data)
      ))
    }
  }

  requestHttpGet(type: ApiType, urlParams: string): Observable<any> {
    return this.http.get(GlobalComponent.API_URL +`${type}/` + urlParams, httpOptions)
  }

  requestHttpPost(type: ApiType, urlParams: string, data: any): Observable<any> {
    return this.http.post(GlobalComponent.API_URL +`${type}/` + urlParams, { form_data: data }, httpOptions).pipe(
      tap(() => this.resetCachedData())
    )
  }

  requestHttpPut(type: ApiType, urlParams: string, id: any, data: any): Observable<any> {
    return this.http.put(GlobalComponent.API_URL +`${type}/` + urlParams + `/${id}`, { form_data: data }, httpOptions).pipe(
      tap(() => this.resetCachedData())
    )
  }

  requestHttpDelete(type: ApiType, urlParams: string, id: number): Observable<any> {
    return this.http.delete(GlobalComponent.API_URL +`${type}/` + urlParams +`/${id}`, httpOptions).pipe(
      tap(() => this.resetCachedData())
    )
  }

  requestUploadFile(type: ApiType, urlParams: string, file: FormData) {
    return this.http.post(GlobalComponent.API_URL + `${type}/` + urlParams, file).pipe(
      tap(() => this.resetCachedData())
    )
  }
  

  // Users API
  getEmployeeData(term: string) {
    if (term === "") {
      return of([] as any[]);
    }

    return this.http.post(GlobalComponent.AIO_API + "employee", { search: term }, httpOptions)
      .pipe(
        map((response: any) => {
          const data = response.data
          if (Array.isArray(data)) {
            return data.filter((item: any) => new RegExp(term, "mi").test(`${item.nik} - ${item.employee_name}`)).slice(0, 10)
          } else {
            return ([] as any[])
          }
        })
      );
  }

  searchUsersByPagination(term: string, page: number, pageSize: number, sortColumn: string, sortDirection: SortDirection) {
    return this.requestHttpGet('master', `users/paging?search=${term}&page=${page}&pageSize=${pageSize}&sortColumn=${sortColumn}&sortDirection=${sortDirection}`)
  }

  insertUser(data: any) {
    return this.requestHttpPost('master', `users`, data)
  }

  updateUser(id: number, data: any) {
    return this.requestHttpPut('master', `users`, id, data)
  }

  deleteUser(id: number) {
    return this.requestHttpDelete('master', `users`, id)
  }

  getUserRole() {
    const cacheKey = "roles"
    return this.requestCachedHttpGet('master', 'users/role', cacheKey)
  }

  // Factory Line API
  getFactoryLine() {
    const cacheKey = "factoryLine"
    return this.requestCachedHttpGet('master', `line`, cacheKey)
  }

  searchFactoryLineByPagination(term: string, page: number, pageSize: number, sortColumn: string, sortDirection: SortDirection) {
    return this.requestHttpGet('master', `line/paging?search=${term}&page=${page}&pageSize=${pageSize}&sortColumn=${sortColumn}&sortDirection=${sortDirection}`)
  }

  updateFactoryLine(id: any, data: any) {
    return this.requestHttpPut('master', `line`, id, data)
  }

  insertFactoryLine(data: any) {
    return this.requestHttpPost('master', `line`, data)
  }

  // Area API
  getArea() {
    const cacheKey = "area"
    return this.requestCachedHttpGet('master', `area`, cacheKey)
  }

  searchAreaByPagination(term: string, page: number, pageSize: number, sortColumn: string, sortDirection: SortDirection) {
    return this.requestHttpGet('master', `area/paging?search=${term}&page=${page}&pageSize=${pageSize}&sortColumn=${sortColumn}&sortDirection=${sortDirection}`)
  }

  updateArea(id: any, data: any) {
    return this.requestHttpPut('master', `area`, id, data)
  }

  insertArea(data: any) {
    return this.requestHttpPost('master', `area`, data)
  }

  // Machine API
  searchMachineByPagination(term: string, page: number, pageSize: number, sortColumn: string, sortDirection: SortDirection) {
    return this.requestHttpGet('master', `machine/paging?search=${term}&page=${page}&pageSize=${pageSize}&sortColumn=${sortColumn}&sortDirection=${sortDirection}`)
  }

  updateMachine(id: any, data: any) {
    return this.requestHttpPut('master', `machine`, id, data)
  }

  insertMachine(data: any) {
    return this.requestHttpPost('master', `machine`, data)
  }
}
